---
Spike Report
---

CORE 3 – BLUEPRINT BASICS
=============

Introduction
------------

In Unreal Engine, a lot of work is completed using Blueprints, a form of
Visual Programming/Scripting.

We have not previously been exposed to such a system.

Goals
-----

1.  In a new Blueprint **FPS** Unreal Engine project, create a
    “Launch-pad” actor, which can be placed to cause the player to leap
    into the air upon stepping on it.

    a.  Play a sound when the character is launched

    b.  Set the Launch Velocity using a variable, which lets you place
        multiple launch-pads with different velocities in the same level

    c.  Add an Arrow Component, and use its rotation to define the
        direction to launch the character

2.  Create a new level using the default elements available to you from
    the Unreal Engine (boxes should suffice), which should be a fun
    little FPS level with many launch-pads that help you access
    different areas.

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K
  Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

-   https://answers.unrealengine.com/questions/349817/how-to-get-rotation-of-arrow-component.html

Tasks undertaken
----------------

Setting up

1.  Start with a FPS Blueprint project, without starter content

Creating a Blueprint

1.  To create a blueprint, place an asset in the scene and click
    “Blueprint/Add script” under details

Creating a Launchpad

1.  To create a Launchpad, drag a cube into the scene, and size it
    appropriately such that it looks like a pad

2.  Add an Arrow Component name Launch Direction by clicking Add
    Component in the details section, and position it to point where you
    want the player to go.

3.  Add an Audio component called Launch Sound component

Editing the Blueprint

1.  Click Edit Blueprint

2.  This will bring you to the object’s blueprint – from here there are
    3 tabs:

    a.  Viewport

        i.  This is provides a more convenient way to edit an object
            outside of the scene

    b.  Construction Script

        i.  This is the constructor of the object that is called when
            the object is **initially placed** in the scene.

    c.  Event Graph.

        i.  This section is where the bulk of the code is written, and
            is constructed in a visual, timeline format.

        ii. To work with this system, right click on an empty area to
            bring up a searchable list of events or functions that can
            be used. You can also drag from the white execution symbols
            of existing events to chain a new node from it.

3.  We begin with an OnComponentBeginOverlap (BoxCollider) node

4.  Using a branch, and Get Player Pawn, we compare using Equals

5.  If the result is true, we Cast To Character using the same Get Pawn
    Player

6.  After, we Launch Player, with Target As Character

7.  We use 2 boolean values (created in the variables section to the
    left), XY Override and Z Override. These must be visible in the
    editor (click the eye to their right)

8.  We also need the launch velocity. To get this we use Get Forward
    Vector, using the Launch Direction Arrow Component

9.  We create a new variable that is visible in the editor, called
    Launch Velocity. We multiply the result of Get Forward Vector by
    this number and use the result as the Launch Velocity

10. Once this completes, we Play (Launch Sound) to add an audio effect.

11. Apply the correct values in the editor, including adding the audio
    file to the component.

12. Place launchpads where they are needed.

What we found out
-----------------

-   How to create a blueprint

-   How to edit a blueprint

-   How to place created blueprints

-   How to interact with other objects in the world with blueprints

 \[Optional\] Recommendations
-----------------------------

There is another method of adding force to an object, called an
**impulse**.
